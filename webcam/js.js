const video = document.querySelector('.player')
const canvas = document.querySelector('.photo')
const ctx = canvas.getContext('2d')
const strip = document.querySelector('.strip')
const snap = document.querySelector('.snap')
// const mediaStream = new MediaStream();
// video.srcObject = mediaStream;

function getVideo() {
  navigator.mediaDevices.getUserMedia({video: true, audio: false})
  .then(localMediaStream => {
    console.log(localMediaStream)
    // video.srcObject = window.URL.createObjectURL(localMediaStream)
    video.srcObject = localMediaStream;
    video.play();
  })
  .catch(err => {
    console.error(`Error ${err}`)
  })
}

function paintToCanvas() {
  const width = video.videoWidth
  const height = video.videoHeight
  console.log(width,height)
  canvas.width = width
  canvas.height = height
 return setInterval(() => {
    ctx.drawImage(video, 0 , 0, width, height)
    // let pixels = ctx.getImageData(0, 0, width, height)
    // pixels = redEffect(pixels)
    // ctx.putImageData(pixels, 0 , 0)
  }, 16);
}

function takePhoto() {
  snap.currentTime = 0;
  snap.play()
  const data = canvas.toDataURL('image/jpeg')
  const link = document.createElement('a')

  link.href = data
  link.setAttribute('download', 'handsome')
  link.innerHTML = `<img src="${data}" alt="Handsome Man" />`
  strip.insertBefore(link, strip.firstChild)
}


function redEffect(pixels) {
  for(let i = 0; i< pixels.length; i+=4) {
    pixels[i + 0] = pixels.data[i + 0] + 100 //RED
    pixels[i + 1] = pixels.data[i + 0] - 500 //GREEN
    pixels[i + 2] = pixels.data[i + 0] * 0.5 //Blue
  }
  return pixels
}

getVideo()

video.addEventListener('canplay', paintToCanvas)