
function playSound(e) {
  const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`)
  const key = document.querySelector(`.key[data-key="${e.keyCode}"]`)
  if (!audio) return
  audio.currentTime = 0
  audio.play()
  // key.addClass('playing')
  key.classList.add('playing');
  // key.classList.remove('playing')
  // key.classList.toggle('playing')
  // setTimeout(() => {

  // }, 0.07)
}

const keys = document.querySelectorAll('.key')

function removeTransition(e) {
  if (e.propertyName !== 'transform') return
  this.classList.remove('playing')
}

keys.forEach(key => {
  key.addEventListener('transitionend', removeTransition)
})

window.addEventListener('keydown', playSound)