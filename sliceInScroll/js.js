function debounce(func, wait = 20, immediate = true) {
  var timeout
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null
      if(!immediate) func.apply(context, args)
    }
    var callNow = immediate && !timeout
    clearTimeout(timeout)
    timeout = setTimeout(later, wait)
    if(callNow) func.apply(context, args)
  }
}

function checkSlide(e) {
  console.count(e)
  console.log(window.scrollY)
  console.log(window.innerHeight)
  console.log(document.querySelector('.box2').offsetTop)
  console.log(document.querySelector('.box2').offsetWidth)
  console.log(document.querySelector('.box2').offsetHeight)
}

window.addEventListener('scroll', debounce(checkSlide))